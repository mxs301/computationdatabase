FROM continuumio/miniconda3

SHELL ["/bin/bash", "-c"]

RUN conda update -yn base -c defaults conda
RUN conda config --add channels conda-forge
RUN conda create -y --name compdata-test mysql-connector-python pytest python pip
ENV PATH /opt/conda/envs/compdata-test/bin:$PATH
RUN python -m pip install localconfig
RUN python -c "import mysql.connector"
