# Welcome to ComputationDatabase

[![Pipeline Status](https://gitlab.com/mxs301/computationdatabase/badges/master/pipeline.svg)](https://gitlab.com/mxs301/computationdatabase/-/commits/master)
[![License](https://img.shields.io/badge/license-MIT-green.svg)](https://gitlab.com/mxs301/computationdatabase/-/blob/master/LICENSE)

## Overview

Computational research involves running many different jobs/calculations on different servers. This repository provides
a Python API for accessing a MySQL database that stores a record of different jobs and groups them by their project.

This is my personal project and API is limited to my use-cases.

Database design:

![database design](images/database_design.png)

## Table description

### Manager

User responsible for managing jobs.

Column name | Description
--- | --- 
ID | unique ID (primary key)
Name | unique name
Email |  optional email address, defaults to ''

### Server

Server where calculations are running.

Column name | Description
--- | --- 
ID | unique ID (primary key)
Name | unique name

### Job

Individual calculation.

Column name | Description
--- | --- 
ID | unique ID (primary key)
Manager_id | reference id of manager who is in charge of this job
Server_id | reference id of server where the job is scheduled
Name | job name
Description | short description of the job
Status | job can be "waiting", "running", "completed", "failed"
Location | path to the project data on the central server
Submitted_on | date and time the job was submitted
Started_on | date and time the job started running
Stopped_on | date and time the job stopped by completion or failure
CPU_hours | total CPU hours used by the calculation

### Project

Jobs are usually submitted as part of a bigger project.

Column name | Description
--- | --- 
ID |  unique ID (primary key)
Name | project name
Description | short description of the project
Started | date and time the project was created
Finished | date and time the project was completed

### Project_content

Assigns jobs to projects. The same job can be part of different projects.

Column name | Description
--- | --- 
Project_id |  reference to project
Job_id | reference to job

## Installation

```bash
git clone https://gitlab.com/mxs301/computationdatabase.git
cd computationdatabase
python -m pip install .
```

## Usage

Class ``CompData``  exposes the full API. Connection is established on construction and should be closed manually by
calling ``CompData.close()``.

```python
from compdata import CompData

# Connection parameters can be entered directly
d = CompData(user="root", host="localhost", port=3603, passwd="password", database="NameOfDatabase")
d.close()
# Or stored in a config file (more secure)
d = CompData(config="path/to/config/file", database="NameOfDatabase")
d.close()
```

``database`` argument is optional. If not provided than a database will have to be created before accessing it.

```python
d = CompData(config="path/to/config/file")
# Create a new database
d.create_database("ComputationalChemistry")
# Register a user and a server
d.register_manager("marat", email="fake.email@knowhere.com")
d.register_server("Dell-XPS15")
```

The manager info stored in the database is used to keep track of who is responsible for the job. Manager and server
names should be unique.

At this point jobs can be registered. Note that each job must be assigned a manager and a server, but default values can
also be set beforehand. Each job will be assigned a unique ID, so their names do not have to be unique

```python
# set default manager and server. The default settings only last while python interpreter is running.
d.set_default(manager="marat", server="Dell-XPS15")
d.register_job(name="H2O-dipole", description="calculate H2O dipole moment")
d.register_job(name="H2O-freq", description="calculate H2O harmonic frequencies", manager="StudentName",
               server="Isambard")
```

Jobs are usually submitted as part of bigger projects. We can register a project, assign a job to it and remove that
assignment.

```python
proj_id = d.register_project(name="H2O benchmark", description="Benchmark different properties of water")
# We can select some jobs and assign them to projects
h2o_jobs = d.select(frm="Job", column='ID', where="Name=H2O-*")
d.project_assign(proj_id, h2o_jobs)
# The link between project and job can also be removed
job_id = d.select(frm="Job", column='ID', where="Name=H2O-freq")
d.delete_project_content(proj_id, job_id)
```

There is a basic mechanism for selecting data from the table through ``CompData.select()``, which simply passes input to
SQL ``SELECT`` call.

```python
my_job_data = d.select(column='*', frm='Job', where='Manager=marat')
```

Entries in ``Job`` table can be update using ``CompData.job_update()``. All tables can be updated manually
with ``CompData.execute()``.

```python
last_job = d.latest("Job", n=1)
d.job_update(last_job, {"Status": "completed", "CPU_hours": 1000})
```

Deletion of table entries should be done through API calls. Tables have clean up mechanism, for example removing a job
or project also deletes relevant entries in ``Project_content``.

```python
d.delete_job(job_ids)
d.delete_project(proj_ids)
d.delete_manager(manager_ids)
d.delete_server(server_ids)
```

For advanced operations, commands can be passed directly to SQL console using ``CompData.execute()``.

```python
my_job_data = d.execute("SELECT * FROM Job WHERE Manager=marat")
```

## License

MIT License, see LICENSE

## Author

Marat Sibaev
