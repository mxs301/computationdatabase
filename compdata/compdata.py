import mysql.connector as mysql
from mysql.connector.errors import DatabaseError
from configparser import NoSectionError
from localconfig import LocalConfig
from pathlib import Path


class CompData:
    """

    Attributes
    ----------
    :param connection: mysql.connector.MySQLConnection object performs all SQL operations.

    Class Attributes
    ----------------
    :param TABLE_SCHEMA_MANAGER: schema for the Manager table
    :param TABLE_SCHEMA_SERVER: schema for the Server table
    :param TABLE_SCHEMA_PROJECT: schema for the Project table
    :param TABLE_SCHEMA_JOB: schema for the Job table
    :param TABLE_SCHEMA_PROJECT_CONTENT: schema for the Project_content table
    """
    TABLE_SCHEMA_MANAGER = """
    Manager (
        ID INT AUTO_INCREMENT PRIMARY KEY,
        Name VARCHAR(255) NOT NULL UNIQUE,
        email VARCHAR(255) NOT NULL DEFAULT ''
    )
    """
    TABLE_SCHEMA_SERVER = """
    Server (
        ID INT AUTO_INCREMENT PRIMARY KEY,
        Name VARCHAR(255) NOT NULL UNIQUE
    )
    """
    TABLE_SCHEMA_PROJECT = """
    Project (
        ID INT AUTO_INCREMENT PRIMARY KEY,
        Name VARCHAR(255) NOT NULL UNIQUE,
        Description TEXT NOT NULL,
        Started DATETIME NOT NULL DEFAULT NOW(),
        Finished DATETIME
    )
    """
    TABLE_SCHEMA_JOB = """
    Job (
        ID BIGINT AUTO_INCREMENT PRIMARY KEY,
        Name VARCHAR(255) NOT NULL,
        Description VARCHAR(255) NOT NULL DEFAULT '',
        Server_id INT NOT NULL,
        Manager_id INT NOT NULL,
        Status VARCHAR(20) NOT NULL DEFAULT 'waiting',
        Path VARCHAR(255) NOT NULL DEFAULT '',
        Submitted_on DATETIME DEFAULT NOW(),
        Started_on DATETIME,
        Stopped_on DATETIME,
        CPU_hours DOUBLE NOT NULL DEFAULT 0,
        CONSTRAINT
        FOREIGN KEY (Server_id)
            REFERENCES Server(ID)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
        CONSTRAINT
        FOREIGN KEY (Manager_id)
            REFERENCES Manager(ID)
            ON DELETE CASCADE
            ON UPDATE CASCADE
    )
    """
    TABLE_SCHEMA_PROJECT_CONTENT = """
    Project_content (
        project_id INT,
        job_id BIGINT,
        CONSTRAINT
        FOREIGN KEY (project_id)
            REFERENCES Project(ID)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
        CONSTRAINT
        FOREIGN KEY (job_id)
            REFERENCES Job(ID)
            ON DELETE CASCADE
            ON UPDATE CASCADE
    )
    """

    def __init__(self, **kwargs):
        """
        Initialise CompData by creating a connection to the server and opening a database.

        Connecting to the database requires at least user name, host and passwd.

        They can be passed directly to the constructor

        .. code-block:: python

            d = CompData(user="UserName", passwd="SecretPassword", host="localhost")

        Or stored in a config file

        .. code-block::

            [MySQL Connect]
            user = UserName
            passwd = SecretPassword
            host = 127.0.0.1
            port = 3306
            compress = False
            database = Test

        And passed to the constructor

        :param user: user name for logging in to MySQL server
        :param passwd: password
        :param host: host name, defaults to "localhost"
        :param database: name of the database to open
        :type config: Path|string
        :param config: Config file with all information that should be passed to
            mysql.connector.connect. Any duplicate kwargs that are passed directly will take
            precedence.
        :param kwargs: any extra parameters are passed to mysql.connector.connect
        """
        self.connection_parameters = {}
        path_to_config_file = kwargs.pop("config", None)
        if path_to_config_file:
            config = LocalConfig()
            if not Path(path_to_config_file).exists():
                raise FileNotFoundError(f"Config file {path_to_config_file} not found")
            config.read(path_to_config_file)
            try:
                for key, value in config.items("MySQL Connect"):
                    self.connection_parameters[key] = value
            except NoSectionError as e:
                raise e
        for key, value in kwargs.items():
            self.connection_parameters[key] = value
        self.connection = mysql.connect(**self.connection_parameters)

    def close(self):
        """
        Closes the connection. This must be called manually.
        :return:
        """
        self.connection.close()

    def drop_database(self, database_name):
        """
        Drops database if it exists, nothing happens if it doesnt
        :param database_name: name of the database
        :return: true if database was dropped, false if DatabaseError was raised
        """
        cursor = self.connection.cursor()
        success = True
        try:
            cursor.execute(f"DROP DATABASE {database_name}")
        except DatabaseError:
            success = False
        finally:
            cursor.close()
        return success

    def create_database(self, database_name):
        """
        Creates a new database. Database is empty and before adding jobs a manager and a server
        must be assigned.

        :param database_name: name of the database
        :return:
        """
        cursor = self.connection.cursor()
        cursor.execute(f"CREATE DATABASE {database_name}")
        cursor.close()
        self.connection.database = database_name
        self._create_tables()

    def _create_tables(self):
        cursor = self.connection.cursor()
        for table in (self.TABLE_SCHEMA_MANAGER, self.TABLE_SCHEMA_SERVER, self.TABLE_SCHEMA_JOB,
                      self.TABLE_SCHEMA_PROJECT, self.TABLE_SCHEMA_PROJECT_CONTENT):
            cursor.execute(f"CREATE TABLE {table}")
        cursor.close()

    def register_manager(self, name, email=""):
        cursor = self.connection.cursor()
        cursor.execute(f"INSERT INTO Manager (name, email) VALUES ('{name}', '{email}')")
        cursor.close()
