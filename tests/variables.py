from pathlib import Path
import os

THIS_DIR = Path(os.path.abspath(__file__)).parent
TEST_DATABASE_NAME = "test_database"
