import pytest
from compdata import CompData
from variables import TEST_DATABASE_NAME, THIS_DIR
import os


@pytest.fixture(scope="module", autouse=True)
def create_default_database():
    config_file = THIS_DIR / "connect.cfg"
    if not os.getenv("NO_PERMISSION_TO_CREATE_DATABASE"):
        d = CompData(config=config_file)
        d.create_database(TEST_DATABASE_NAME)
        yield
        d.drop_database(TEST_DATABASE_NAME)
        d.close()
    else:
        d = CompData(config=config_file, database=TEST_DATABASE_NAME)
        d._create_tables()
        yield
        d.close()


@pytest.fixture
def connect_with_default_database():
    config_file = THIS_DIR / "connect.cfg"
    data = CompData(config=config_file, database=TEST_DATABASE_NAME)
    cursor = data.connection.cursor()

    def truncate_database():
        cursor.execute("SHOW TABLES")
        tables = cursor.fetchall()
        if tables:
            for table in tables:
                cursor.execute(f"DELETE FROM {table[0]}")

    truncate_database()
    yield data, cursor
    truncate_database()
    cursor.close()
    data.close()


def test_register_manager_one(connect_with_default_database):
    data, cursor = connect_with_default_database
    test_manager = ("Test Name", "test.test@test.com")
    data.register_manager(name=test_manager[0], email=test_manager[1])
    cursor.execute("SELECT * FROM Manager")
    managers = cursor.fetchall()
    assert len(managers) == 1
    managers_without_id = [(m[1], m[2]) for m in managers]
    assert managers_without_id == [test_manager, ]


def test_register_manager_two(connect_with_default_database):
    data, cursor = connect_with_default_database
    test_managers = [
        ("Test Name 1", "test.test1@test.com"), ("Test Name 2", "test.test2@test.com")]
    for test_manager in test_managers:
        data.register_manager(name=test_manager[0], email=test_manager[1])
    cursor.execute("SELECT * FROM Manager")
    managers = cursor.fetchall()
    assert len(managers) == 2
    managers_without_id = [(m[1], m[2]) for m in managers]
    assert managers_without_id == test_managers
