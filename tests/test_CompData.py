from compdata import CompData
from compdata.compdata import NoSectionError
import pytest
from variables import TEST_DATABASE_NAME, THIS_DIR
import os


def test_constructor_from_config_file():
    config_file = THIS_DIR / "connect.cfg"
    c = CompData(config=config_file)
    c.close()


def test_constructor_from_config_file_without_section():
    config_file = THIS_DIR / "no_section.cfg"
    with pytest.raises(NoSectionError):
        c = CompData(config=config_file)
        c.close()


def test_constructor_from_missing_config_file():
    config_file = THIS_DIR / "does-not-exist.cfg"
    with pytest.raises(FileNotFoundError):
        c = CompData(config=config_file)
        c.close()


def test_close():
    config_file = THIS_DIR / "connect.cfg"
    c = CompData(config=config_file)
    c.close()
    assert not c.connection.is_connected()


@pytest.fixture
def connect_without_database():
    config_file = THIS_DIR / "connect.cfg"
    data = CompData(config=config_file)
    cursor = data.connection.cursor()
    yield data, cursor
    cursor.close()
    data.close()


@pytest.mark.skipif(os.getenv("NO_PERMISSION_TO_CREATE_DATABASE"), reason="no permission")
def test_drop_database(connect_without_database):
    data, cursor = connect_without_database
    database_name = "test_drop_database"
    cursor.execute(f"CREATE DATABASE {database_name}")
    assert data.drop_database(database_name)


@pytest.mark.skipif(os.getenv("NO_PERMISSION_TO_CREATE_DATABASE"), reason="no permission")
def test_drop_database_non_existant(connect_without_database):
    data, cursor = connect_without_database
    database_name = "test_database_does_not_exist"
    assert not data.drop_database(database_name)


@pytest.mark.skipif(os.getenv("NO_PERMISSION_TO_CREATE_DATABASE"), reason="no permission")
def test_create_database(connect_without_database):
    data, cursor = connect_without_database
    random_database_name = TEST_DATABASE_NAME
    data.drop_database(random_database_name)
    data.create_database(random_database_name)
    assert data.connection.database == random_database_name
    cursor.execute("SHOW TABLES")
    tables = cursor.fetchall()
    data.drop_database(random_database_name)
    assert set(tables) == {("Manager",), ("Server",), ("Project",), ("Job",), ("Project_content",)}


