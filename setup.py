#!/usr/bin/env python

from distutils.core import setup

setup(
    name="ComputationDatabase-mxs301",
    version='0.0.0',
    author="Marat Sibaev",
    email="sibaev.marat@gmail.com",
    url="https://gitlab.com/mxs301/computationdatabase",
    description="",
    long_description="",
    packages=['compdata'],
    install_requires=['mysql-connector-python', 'pytest', 'localconfig'],
    license="MIT"
)
